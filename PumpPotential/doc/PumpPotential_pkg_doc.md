<!-- Equation (using $$..$$) automatic numbering, use the tag \nonumber to deactivate for an individual equation -->
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { 
      equationNumbers: { 
            autoNumber: "all",
            formatNumber: function (n) {return 'Eq.'+n}
      } 
  }
});
</script>

# Package PumpPotential for vle-2.0.2

The **PumpPotential** package provide two models called **PumpPotential** and **AvailableWater** and their corresponding configurations in order to facilitate the use of the model.  
The **PumpPotential** model estimates the maximum daily water amount a pump can extract.  
The **AvailableWater** model manage a temporary water tank where the irrigation water can be taken from.

# Table of Contents
1. [Package dependencies](#p1)
2. [Atomic model PumpPotential](#p2)
    1. [Configuring a PumpPotential Model](#p2.1)
        1. [Dynamics settings](#p2.1.1)
        2. [Parameters settings](#p2.1.2)
        3. [Input settings](#p2.1.3)
        4. [Output settings](#p2.1.4)
        5. [Observation settings](#p2.1.5)
        6. [Available configurations](#p2.1.6)
    2. [Details](#p2.2)
2. [Atomic model AvailableWater](#p3)
    1. [Configuring a AvailableWater Model](#p3.1)
        1. [Dynamics settings](#p3.1.1)
        2. [Parameters settings](#p3.1.2)
        3. [Input settings](#p3.1.3)
        4. [Output settings](#p3.1.4)
        5. [Observation settings](#p3.1.5)
        6. [Available configurations](#p3.1.6)
    2. [Details](#p3.2)

---

## Package dependencies <a name="p1"></a>

List of required external packages (with link to distribution when available).

* [vle.discrete-time](http://www.vle-project.org/pub/2.0/vle.discrete-time.tar.bz2)

--- 

## Atomic model PumpPotential <a name="p2"></a>

The **PumpPotential** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) estimate water flows from daily groundwater height and electricity duration.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#PumpPotential_panel'>Show/Hide Model Details</button>
<div id="PumpPotential_panel" class="collapse">

### Configuring a PumpPotential Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : PumpPotential
* **name** : PumpPotential

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **Altitude** | double |[] default:100 | soil surface altitude (m above sea level)|
| **WellDepth** | double |[]  default:100 | well depth (m below soil surface)|
| **CoeffA** | double |[]  default:79.9308 | coefficient A of the PumpFlow equation (m3 of water/m ground depth/h) |
| **CoeffB** | double |[]  default:-0.728 | coefficient B  of the PumpFlow equation (-) |
| **CoeffC** | double |[]  default:1 | coefficient C  of the PumpFlow equation (-) |
| **Sy** | double | [] default:0.005  | Specific yield (-) |
| **GWpixelArea** | double |[]  default:10000 | surface of the groundwater pixel (m2) |


#### Input settings <a name="p2.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info 

* **H :** groundwater level (m above sea level) (double) (nosync)  
* **PumpingDuration :** electricity availability (hour/day) (double) (sync)  

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **WaterTableDepth :** water table depth (m below soil surface) (double)  
* **PumpFlow :** maximum pump flow (m3 of water/hour) (double)  
* **PumpMaxVol :** maximum water volume reachable by the pump (m3 of water) (double)  
* **AvailableWater :** maximum available water (m3 of water /day) (double)  

#### Observation settings <a name="p2.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

* **WaterTableDepth :** water table depth (m below soil surface) (double)  
* **PumpFlow :** maximum pump flow (m3 of water/hour) (double)  
* **PumpMaxVol :** maximum water volume reachable by the pump (m3 of water) (double)  
* **AvailableWater :** maximum available water (m3 of water /day) (double)  

#### Available configurations <a name="p2.1.6"></a>

* PumpPotential/PumpPotential : set all parameters with default values

### Details <a name="p2.2"></a>

#### State variables equations <a name="p2.2.1"></a>

> $$WaterTableDepth(t)=\max\left(
  \begin{array}{@{}ll@{}}
    1.0 \\
    Altitude - H(t-1)
  \end{array}\right)
\label{eq:WaterTableDepth}$$

> $$PumpFlow(t)=\left\{
  \begin{array}{@{}ll@{}}
    0.0, & \text{if}\ WaterTableDepth(t)>WellDepth \\
    \max\left(
  \begin{array}{@{}ll@{}}
    0.0 \\
    CoeffA * WaterTableDepth(t)^{CoeffB}
  \end{array}\right), & \text{otherwise}
  \end{array}\right.
\label{eq:PumpFlow}$$

> $$PumpMaxVol(t)=\max\left(
  \begin{array}{@{}ll@{}}
    0 \\
    (WellDepth - WaterTableDepth(t)) * Sy * GWpixelArea
     \end{array}\right)
\label{eq:PumpMaxVol}$$




> $$AvailableWater(t)=\min\left(
  \begin{array}{@{}ll@{}}
    PumpMaxVol(t) \\
    \max\left(
  \begin{array}{@{}ll@{}}
    0.0 \\
    PumpFlow(t) * PumpingDuration(t) * CoeffC
  \end{array}\right)
  \end{array}\right)
\label{eq:AvailableWater}$$

</div>

--- 

## Atomic model AvailableWater <a name="p3"></a>

The **AvailableWater** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) estimate ....

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#AW_panel'>Show/Hide Model Details</button>
<div id="AW_panel" class="collapse">

### Configuring a AvailableWater Model <a name="p3.1"></a>

#### Dynamics settings <a name="p3.1.1"></a>

To define the dynamic:

* **library** : PumpPotential
* **name** : AvailableWater

#### Parameters settings <a name="p3.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **n** | integer |[] default:1 | maximum number of days for water storage |


#### Input settings <a name="p3.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info 

* **GWVol :** groundwater volume (m3 of water) (double) (nosync)  
* **Irrigation :** daily irrigation (m3 of water) (double) (nosync)  
* **PumpFlow :** maximum available water (m3 of water /day) (double) (sync)  

#### Output settings <a name="p3.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **CurrentAvailableWater :** maximum available water (m3 of water /day) (double)  

#### Observation settings <a name="p3.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

* **CurrentAvailableWater :** maximum available water (m3 of water /day) (double)  

#### Available configurations <a name="p3.1.6"></a>

List and information about all configuration metadata files

* PumpPotential/AvailableWater : set all parameters with default values

### Details <a name="p3.2"></a>

#### State variables equations <a name="p3.2.1"></a>


A buffer of size **n** stores the last *n* values of **PumpFlow**.
When Irrigation(-1) > 0, all values in this buffer are reset to 0.


> $tmp\_CurrentAvailableWater(t) = sum\{buffer\}$
$tmp\_CurrentAvailableWater = min\{tmp\_CurrentAvailableWater, GWVol(-1)\}$

> $CurrentAvailableWater(t) = tmp\_CurrentAvailableWater/2.0$
</div>


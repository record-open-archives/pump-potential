/**
  * @file PumpPotential.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <algorithm>    // std::max
namespace vd = vle::devs;

namespace vv = vle::value;

namespace PumpPotential {

using namespace vle::discrete_time;

class PumpPotential : public DiscreteTimeDyn
{
public:
PumpPotential(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    WaterTableDepth.init(this, "WaterTableDepth", evts);
    PumpFlow.init(this, "PumpFlow", evts);
    PumpMaxVol.init(this, "PumpMaxVol", evts);
    AvailableWater.init(this, "AvailableWater", evts);
    H.init(this, "H", evts);
    PumpingDuration.init(this, "PumpingDuration", evts);
    getOptions().syncs.insert(std::make_pair("PumpingDuration", 1)); // force sync for in input
    mAltitude = (evts.exist("Altitude")) ? vv::toDouble(evts.get("Altitude")) : 100.;
    mWellDepth = (evts.exist("WellDepth")) ? vv::toDouble(evts.get("WellDepth")) : 100.;
    mCoeffA = (evts.exist("CoeffA")) ? vv::toDouble(evts.get("CoeffA")) : 79.9308;
    mCoeffB = (evts.exist("CoeffB")) ? vv::toDouble(evts.get("CoeffB")) : -0.728;
    mCoeffC = (evts.exist("CoeffC")) ? vv::toDouble(evts.get("CoeffC")) : 1;
    gw_Sy = (evts.exist("Sy")) ? vv::toDouble(evts.get("Sy")) : 0.005;
    mGWpixelArea = (evts.exist("GWpixelArea")) ? vv::toDouble(evts.get("GWpixelArea")) : 10000.;


}

virtual ~PumpPotential()
{}

void compute(const vle::devs::Time& /*t*/)
{
    WaterTableDepth = std::max(1.0, mAltitude - H(-1));
    PumpFlow = (WaterTableDepth()>mWellDepth) ? 0.0 : std::max(0.0,mCoeffA * std::pow(WaterTableDepth(), mCoeffB));
    
    PumpMaxVol = std::max(0., (mWellDepth - WaterTableDepth()) * gw_Sy * mGWpixelArea);
    
    AvailableWater = std::min(std::max(0.0, PumpFlow() * PumpingDuration() * mCoeffC ), PumpMaxVol());

}

    Var WaterTableDepth;
    Var PumpFlow;
    Var PumpMaxVol;
    Var AvailableWater;
    Var H;
    Var PumpingDuration;
    double mAltitude;
    double mWellDepth;
    double mCoeffA, mCoeffB, mCoeffC;
    double gw_Sy;
    double mGWpixelArea;

};

} // namespace PumpPotential

DECLARE_DYNAMICS(PumpPotential::PumpPotential)


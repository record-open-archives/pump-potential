/**
  * @file AvailableWater.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <numeric>
#include <algorithm>
namespace vd = vle::devs;

namespace vv = vle::value;

namespace PumpPotential {

using namespace vle::discrete_time;

class AvailableWater : public DiscreteTimeDyn
{
public:
AvailableWater(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    CurrentAvailableWater.init(this, "CurrentAvailableWater", evts);
    PumpFlow.init(this, "PumpFlow", evts);
    getOptions().syncs.insert(std::make_pair("PumpFlow", 1)); // force sync for in input
    GWVol.init(this, "GWVol", evts);
    Irrigation.init(this, "Irrigation", evts);

    n = (evts.exist("n")) ? vv::toInteger(evts.get("n")) : 1;

    for (int i = 0; i < n; ++i) {
        mBuffer.push_back(0.);
    }

}

virtual ~AvailableWater()
{}

void compute(const vle::devs::Time& /*t*/)
{
    double tmp_CurrentAvailableWater;
    if (Irrigation(-1) > 0) {
        for (int i = 0; i < n; ++i) {
            mBuffer[i] = 0.;
        }
    }
    
    std::rotate(mBuffer.rbegin(),
                    mBuffer.rbegin() + 1,
                    mBuffer.rend());
    mBuffer[0] = PumpFlow();
    tmp_CurrentAvailableWater = std::accumulate(mBuffer.begin(), mBuffer.end(), 0.0);
    
    tmp_CurrentAvailableWater = std::min(tmp_CurrentAvailableWater, GWVol(-1));
    
    CurrentAvailableWater = tmp_CurrentAvailableWater/2.;

}

    Var CurrentAvailableWater;
    Var PumpFlow;
    Var GWVol;
    Var Irrigation;
    int n;
    std::vector< double > mBuffer;
};

} // namespace PumpPotential

DECLARE_DYNAMICS(PumpPotential::AvailableWater)


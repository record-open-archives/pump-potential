
# Pump Potential

> A model to estimate a pump water flows from daily groundwater height and electricity duration.

This model has been developped during the [Atcha project](https://www6.inrae.fr/atcha/).  

The documentation of the model is available at https://record-open-archives.pages.mia.inra.fr/pump-potential/

## Requirements

* [VLE-2.0.2](https://github.com/vle-forge/vle/releases/tag/v2.0.2) ([requirements](https://github.com/vle-forge/vle/tree/master2.0#requirements))
* VLE packages
  * [vle.discrete-time](https://github.com/vle-forge/packages/tree/82ee9dfbae1fb24cdbdb39ea9ff7eee19eb062fe/vle.discrete-time)
  * [vle.reader](https://github.com/vle-forge/packages/tree/82ee9dfbae1fb24cdbdb39ea9ff7eee19eb062fe/vle.reader)
  * [vle.tester](https://github.com/vle-forge/packages/tree/82ee9dfbae1fb24cdbdb39ea9ff7eee19eb062fe/vle.tester)

## Build/installation instructions

### Local build

Once the above requirements are met install the Ambhas_Cpp VLE package by running the following command from the root directory of this project

```shell
vle -P PumpPotential clean rclean configure build test
```
### Using docker

Alternativly a [docker](https://www.docker.com/get-started/) image satisfying all the above requirements is available in a public docker registry at registry.forgemia.inra.fr/record/record@sha256:93cb949a4228b4da336490900b523483503a07ab07eb8a3b438b66c7e75919e5.  
Once docker is installed, run an interactive container with the following command from the root directory of this project

```shell
docker run \
       --rm \
       -it \
       --user root \
       -v "$(pwd):/work" \
       registry.forgemia.inra.fr/record/record@sha256:93cb949a4228b4da336490900b523483503a07ab07eb8a3b438b66c7e75919e5 \
       bash
```

Then from inside the container run the commands

```shell
vle -P PumpPotential clean rclean configure build test
```

## Licence

GPLv3 or later. See the file PumpPotential/Licence.txt. Some files are under a different license. Check the headers for the copyright info.